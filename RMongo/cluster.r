#CLUSTERING

raw_Data_no_dup <- raw_data[!duplicated(raw_data[,-1]), ]

tsne_data <- tsne::tsne(X = raw_Data_no_dup[, -1])
fit <- kmeans(tsne_data, 20)
cluster::clusplot(tsne_data, fit$cluster, color = TRUE, shade = TRUE, lines = 0, labels = 4)

tsne_data_1 <- Rtsne::Rtsne(raw_Data_no_dup[, -1])
fit_1 <- kmeans(tsne_data_1$Y, 20)
cluster::clusplot(tsne_data_1$Y, fit_1$cluster, color = TRUE, shade = TRUE, lines = 0, labels = 4)


pca_coef <- prcomp(raw_Data_no_dup[,-1])
pca_data <-  predict(pca_coef, raw_data[,-1])
fit_pca <- kmeans(pca_data[,1:2], 20)
cluster::clusplot(pca_data[,1:2], fit_pca$cluster, color = TRUE, shade = TRUE, lines = 0, labels = 4)
