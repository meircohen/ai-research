getReadingTime <- function(email){
    actions <- data.frame(email$actions)
    res <- actions$actionData[actions$actionType == "Read"]
    if(length(res) == 0){
        return(0)
    } 
    
    return(res)
}

isFalgged <-  function(email){
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) { 
        return(FALSE)
    }
    
    actions <- actions[with(actions, order(actionTime, decreasing = T)), ]
    if (!is.na(actions$flag[1]))
        return(TRUE)
    
    return(FALSE)
}

isTask <-  function(email){
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }

    actions <- actions[with(actions, order(actionTime, decreasing = T)), ]
    return(actions$markedAsTask[1])
}

isCategorized <-  function(email){
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }
    
    actions <- actions[with(actions, order(actionTime, decreasing = T)), ]
    if (!is.na(actions$categories[1]))
        return(TRUE)
    
    return(FALSE)
}

isSelf <- function(email){
    return(email$self)
}

isForward <- function(email) {
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }
    
    res <- any(actions$actionType == "FW")
    return(res)   
}

isReply <- function(email) {
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }
    
    res <- any(actions$actionType == "Reply")
    return(res)   
}

isReplyAll <- function(email) {
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }
    
    res <- any(actions$actionType == "Reply All")
    return(res)   
}

isDeleted <- function(email){
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }
    
    res <- any(actions$actionType == "Delete")
    return(res)
}

isFiled <- function(email){
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(FALSE)
    }
    
    res <- any(actions$actionType == "Filed")
    return(res)
}

isScored <- function(email){
    actions <- data.frame(email$actions)
    if (nrow(actions) == 0) {
        return(NA)
    }
    
    actions <- actions[actions$actionType == "Score", ]
    actions <- actions[with(actions, order(actionTime, decreasing = T)), ]
    if (nrow(actions) > 0) {
        return(actions$userScore[1] > 500)
        # if (nrow(actions) %% 2 == 0) {
        #    return(actions$userScore[1])
        # }
    }
    
    return(NA)
}

runHeuristics <- function(emails){
    for (i in nrow(emails):1) {
        res <-  clacHeuristics(emails[i,])
        if (is.na(res)) {
            emails <-  emails[-i,]
            next()
        }
        emails$kmScore[i] = res
    }
    return(emails)
}

clacHeuristics <- function(email) {
    res = NA
    if (!is.na(isScored(email))) {
        return(isScored(email))
    }
    
    if (isDeleted(email)) {
       res = FALSE 
    }
    
    if (!email$isConversation && (email$isSocial || email$isNoReply || email$isAdvertisment)) {
        res = FALSE
    }
    
    if (isCategorized(email) || isFalgged(email) || isForward(email) || isReply(email) || isReplyAll(email)) {
       res = TRUE 
    }
    
    if (email$isConversation && email$isConversationOwner && grepl(pattern = email$userId, x = email$toRecepients)) {
        res = TRUE
    }
    
    if (is.null(res) && (email$receivedTime + days(7)) <= as.POSIXct(Sys.Date())) {
        res = FALSE
    }

    return(res)
}





