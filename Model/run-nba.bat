if not exist "Models\NBA" mkdir Models\NBA
if not exist "Logs\NBA" mkdir Logs\NBA

for %%i in (c:\Temp\Data\nba_data\*.json) do (
  c:\R\bin\R.exe CMD BATCH --slave "--args -i %%i -o Models\NBA\%%~ni.json" batchNBA.R Logs\NBA\%%~ni.log
)