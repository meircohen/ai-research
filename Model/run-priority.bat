if not exist "Models\Priority" mkdir Models\Priority
if not exist "Logs\Priority" mkdir Logs\Priority


for %%i in (c:\Temp\Data\priority_data\*.json) do (
  c:\R\bin\R.exe CMD BATCH --slave "--args -i %%i -o Models\Priority\%%~ni.json -n" batch.R Logs\Priority\%%~ni.log
)